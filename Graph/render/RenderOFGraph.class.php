<?php
/**
* Open Flash Chart renderer for Graphs in ActiveCore
* http://teethgrinder.co.uk/open-flash-chart/
 *
 * @license GPL
 * @version $Id: RenderOFGraph.class.php 7180 2011-08-16 05:41:32Z jbaker $
 * @package ActivistCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */


/**
*
* Open Flash Chart renderer for Graphs in ActiveCore
* http://teethgrinder.co.uk/open-flash-chart/
*
* @package ActiveCore
* @creator Jonah
* @liscense GPL
*
*/

class RenderOFGraph {

    /*
    * @function __construct
    * @creator Jonah
    * @version 1.0  6/23/07 12:02 PM
    * @version 1.1  7/18/08 1:03 PM JB
    *  - added http://url/?flush_graph_cache to refresh graph cache.
    *  - added 'bar' graph type.
    *
    * @abstract
    * Make the chart from the ACGraph data object.
    */

    function __construct(&$graph) {
        switch($graph->type) {
            case"pie":
                $this->graph = $graph;
                $this->load_pie();
                break;
            default:
                $this->graph = $graph;
                $this->min=100000;
                $this->load($graph->data, $graph->legend);
        }
    }

    /*
    * Make a pie graph
    */
    function load_pie() {
        $character_limit = 16;
        // get the total first.
        foreach($this->graph->data AS $count=>$pt) {
            $this->total = $this->total + $pt[point];
        }

        if($this->graph->debug) show_error("The total is $this->total");
        foreach($this->graph->data AS $count=>$pt) {
            $this->points[] = $pt[point];
            // Decide if you are going to show the label, some numbers are too small.
            if(count($this->graph->data) > 15) {
                $percent = $pt[point]/$this->total;
                if($percent>.02) {
                    $this->labels[] = substr($pt[label],0,$character_limit);
                } else {
                    $this->labels[] = ' ';
                }
            } else {
                $this->labels[] = substr($pt[label],0,$character_limit);
            }
            $this->links[] = $pt[link];
        }

        // What does this next thing do?  JB  2/23/09 12:34 PM
        $i=0;
        foreach($this->points as $pt) {
            if($this->total) $this->values[] = round($pt/$this->total,3)*100;
            if($this->values[$i]<2) {
              $this->labels[$i] = ' ';
            }
            $i++;
        }
        if($this->graph->debug) da($this->labels);
    }

    /*
    * @function load
    * @abstract
    * Load the data from the ACGraph object in order to create the graph data
    * to feed the flash file.
    */
    function load($data, $legend) {
        if(!count($this->labels)) $set_labels = true;
        if(isset($data[0][point])) {
            foreach($data AS $pt) {
                $this->ydata[] = $pt[point];
                if($pt[point] > $max ) $max = $pt[point];
                if($pt[point] < $this->min) $this->min = $pt[point];
                if($set_labels) $this->labels[] = $pt[label];
                if($pt[link]) $this->links[] = $pt[link];
            }
        }
        if(!$this->overhead) $this->overhead = 1000;
        // Attempt to set the max to determine how high to put the Y axis numbers.
        if($max>$this->max) {
          $this->overhead = $max * .1;
          $roundlength = 1 - strlen($max) ;
          if($this->overhead>1) {
             $this->max = $this->max + $this->overhead;
          }
          $this->max = round($max+5,$roundlength);
        }
        if($this->max<20) $this->max = 20;
        $this->min = round($this->min,-3) - 1000;
        // want it to read from left to right.
        $this->ydata = array_reverse($this->ydata);
        $this->labels = array_reverse($this->labels);
    }

    /**
    * Output the files to the browser necesarry to show the graph.
    * @author Jonah  12/18/09 9:29 AM
    */

    function show() {
        global $DOCUMENT_ROOT;
        // use the chart class to build the chart:
        // Call different files depending on the version you want to be running.
        if($this->graph->ofchart=19) {
          require_once($DOCUMENT_ROOT.AC_LIB_DIR.'/ofchart/php/open-flash-chart.php');
        } else {
          require_once($DOCUMENT_ROOT.AC_LIB_DIR.'/ofchart/OpenFlashChart.php');
        }

        // Get started with the graph object.
        $g = new OFGraph();

        // These are some nice colors that work together if you are looking to arbitrarily add colors.
        $colors = array("#0c9fd6","#FF6600","#938b6b","#CC0000","#3399FF","#666666","#009900","#993300","#6666CC","#0066CC");

        $g->title($this->graph->legend,'{font-size: 12px;font-weight:bold;}');

        // Change the 'steps' to leave space if we are trying to show a lot of labels on the x axis.
        if(is_numeric($this->graph->x_step)) {
            // manually set the x steps
            $this->x_step = $this->graph->x_step;
        } else {
            // Try to automatically determine the steps based on the number of labels.
            $pcount = count($this->labels);
            if($pcount > 30) {
                $this->x_step = 10;
            } elseif($pcount<5) {
                $this->x_step = 1;
            } else {
                $this->x_step = 2;
            }
        }

        switch($this->graph->type) {

            case"pie":
                // alpha, border color, font style, gradient

                $g->pie(60,"#999999",'{font-size:12px;color:#404040;}',false);

                $g->bg = '#FFF';
                $g->pie_slice_colours( $colors );
                $g->pie_values( $this->values, $this->labels, $this->links );
                if($this->currency) {
                  $g->set_tool_tip( '#val#<br>#x_label#' );
                } else {
                  $g->set_tool_tip( '#val#%<br>#x_label#' );
                }
                break;
            case"bar":
                $g = $this->bar($g);
                break;
            default: // line or area graph
                $g->inner_bg_colour = "#FFFFFF";
                $g->outer_bg_colour = "#FFFFFF";
                $g->x_grid_colour = "#FFFFFF";

                // area graph, either single or multi mode
                if(isset($this->graph->data[0][point])) { // single data set
                  $g->set_data( $this->ydata );
                  if(is_array($this->links)) {
                    $g->set_links($this->links);
                  }
                  //
                  $g->area_hollow(2,3,25,'CC0000','',10,"#FF9999");

                } else { // array of data sets.
                  $s=0;

                  foreach($this->graph->data AS $set) {
                    unset($this->ydata);
                    $this->load($set[data], $set[legend]);
                    $g->set_data($this->ydata);
                    $g->set_links($this->links);
                    // 2, circle diameter, area alpha, line color, legend, legend font size, FF
                    $g->area_hollow(2,1,30,$colors[$s],$set[legend],10,$colors[$s]);
                    $s++;
                  }
                  $this->min = 0;  // if you're looking at multiple graphs, it's useful to see their relative values.
                }
         }

        // Set some variables for area, line, and bar graphs.
        if($this->graph->type!="pie") {
                $g->set_x_labels($this->labels);
                $g->set_y_max( $this->max );
                $g->set_y_min( 0 );
                $g->y_label_steps( 5 );
                // label each point with its value (size, color, angle code, label steps);
                $g->set_x_label_style( 10,'0x666666',2,$this->x_step);
        }

        /**
        * Now create the data file in a file cache
        */
        require_once "Cache/Lite.php";
        if($this->values) {
            $string = dirify(substr(implode("_",$this->values),0,40));
        } else {
            $string = dirify(substr(implode("_",$this->labels),0,40));
        }

        $fname = dirify($this->graph->legend)."_".count($this->labels)."_".$string;

        $options = array(
            'cacheDir' => $DOCUMENT_ROOT.AC_LIB_DIR.'/ofchart/tmp/',
            'lifeTime' => 9200,
            'pearErrorMode' => CACHE_LITE_ERROR_DIE
        );
        $cache = new Cache_Lite($options);

        if ($data = $cache->get($fname)) { // cache exists, use it
            // do nothing, the open_flash_chart_object code will reference cache file.
        }

        if(!$data || $_REQUEST['flush_graph_cache'] || $this->graph->debug) {
            // No valid cache found (you have to make and save the page)

            ob_start();
            $data = $g->render();
            $cache->save($data);


            $file = fopen ($DOCUMENT_ROOT.AC_LIB_DIR.'/ofchart/tmp/'.$fname.".txt", "w");
            fwrite($file, $data);
            fclose($file);
            $datacache = ob_get_clean();

        }

        open_flash_chart_object($this->graph->width,$this->graph->height,'http://'.$_SERVER['SERVER_NAME'].'/core/mods/ofchart/tmp/'.$fname.".txt",false);

    }

    /**
    * Takes the graph object and returns it with a bar graph object loaded for rendering.
    *
    * @version 0.1 alpha Jonah  7/18/08 12:40 PM
    *
    */

    function bar(&$g) {

        $bar = new bar_outline( 50, '#FF9999', '#CC0000' );

        $bar->data = $this->ydata;

        //
        // BAR CHART:
        //
        //$g->set_data( $data );
        //$g->bar_filled( 50, '#9933CC', '#8010A0', 'Page views', 10 );
        //
        // ------------------------
        //
        $g->data_sets[] = $bar;
        $g->x_grid_colour = "#FFFFFF";
        // $g->set_x_labels($this->labels);
        // $g->set_x_axis_steps( 2 );
        return $g;

    }
}
