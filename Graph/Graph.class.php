<?php
/**
 * Generate flash graphs.
 *
 * @license LGPL
 * @version $Id: Graph.class.php 8052 2014-06-13 22:19:43Z jbaker $
 * @package ActivistCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */

/**
* Required files
*/
require(dirname(__FILE__)."/GraphConfig.class.php");
/**
*  A wrapper for the Open Flash Chart open source flash charting engine:
*  http://teethgrinder.co.uk/open-flash-chart/
*  Technically could support different charting engines, like google chart.
*  Newer version of Open Flash Chart use more javascript to display the charts and load the data.
*  This is still based on the old version, and the chart data files are cached for 1 hour by default,
*  using the 'Legend' and some of the data array as the key to the chart.
*
*  @author jonah@activistinfo.org
*  @version 1.1  6/23/07 12:08 PM
*
*  Troubleshooting is sometimes best done on the cache files, at /core/mod/ofchart/tmp/
*
*  Known Problems:
*    - Pie graphs aren't smart about long labels or orverlaping labels, ACGraph
*      simply removes the labels for very small percentages.
*
*  Example:
*  $data = array(0=>array('point'=>'01/01/2008','label'=>'34'));
*  $legend = 'A test graph';
*  $g = new ACGraph($data,$legend);
*  $g->render();
*/

class ACGraph extends ACGraphConfig {
    var $type = 'line'; // supported types are line, pie
    var $debug = false;
    
    /*
    *
    *  @var data = array of 'point' and 'label' info.
    *  @var legend = Graph legend
    */
    function __construct($data=false,$legend=false, $opts=false) {
        if(!$legend) {
            $this->legend = "You must specify a legend with the second variable of ACGraph.";
        } else {
            $this->legend = $legend;
        }

        // Declare known graph renderes, (ex. ofgraph, ofgraph2, google)
        $this->renderers = array("ofgraph"=>"RenderOFGraph","googleapi"=>"RenderGoogleGraph");

        if($opts[width]) {
              $this->width = $opts[width];
              $this->height = $opts[height];
        } else {
              if(!$this->width) $this->width=500;
              if(!$this->height) $this->height=200;
        }
        
        if(is_string($data)) {
            // assume sql
            $data = $this->_dbo->fetchAll($data);
            // print_r($data);
            /*
            foreach($all AS $one) {
                da($one);
            }
            */
        }
        // $this->da($data);
        if($this->pcount=count($data)) {
            if(!is_numeric($data[0][point])) $e[] = "Must give \$data[0][point] array to ACGraph.";
            if(!$data[0][label]) $e[] = "Must give \$data[0][label] array to ACGraph.";
            if(is_array($e)) $this->show_errors($e);
            
            foreach($data AS $key=>$point) {
              // $point['label'] = "Title";
              $data[$key][label] = str_replace('&','and',$point[label]);
            }
            $this->data = $data;
        } else {
            $e = "<div style='border:1px solid #CCCCCC;'>No data given to ACGraph. You must had ACGraph an array of arrays as [point] and [label].
              Then you use the \$graph->render(); to show the graph.</div>";
            // error_log($e);
            echo($e);
            // return false;
        }
    }
    
    function da($in) {
        ?><pre><?php
        print_r($in);
        ?></pre><?php
    }
    function show_errors($in) {
        print_r($in);
    }
    /*
    * Use this to load multiple data sets for bar or line graphs
    */

    function load($data, $legend) {
        // see if we are in single dataset mode, and switch over to another nesting
        //  level in the array.
        if(isset($this->data[0][point])) {
          $set = $this->data;
          unset($this->data);
          $this->data[0]['data'] = $set;
          $this->data[0][legend] = "All";
        }
        $i = count($this->data);
        krsort($data);
        $this->data[$i][data] = $data;
        $this->data[$i][legend] = $legend;
    }

    /**
    * Output the graph to the browser.  Output is file based and cached for one hour by default.
    *
    * @todo - Add google data visualization as a renderer - JB  7/18/11 9:25 PM
    * @version 1.0 Jonah 2008
    */
    function render($type=false) { 
        if(!$type) $type = "googleapi";
        if(in_array($type,array_keys($this->renderers))) { // known renderer
            require_once(dirname(__FILE__)."/render/".$this->renderers[$type].".class.php");
            // The following line needs to be replaced with a GraphRenderer::factory($type); approach once there are rendering choices.
            // JB  7/8/08 4:29 PM
            $r = new $this->renderers[$type]($this);
            $r->show();
        } else {
            $e = "Unknown renderer: $type";
            error_log($e);
            return false;
        }
        $this->showFooter();
    }
    
    /**
    * @version 1.0 Jonah B  6/13/14 3:03 PM
    */
    function showFooter() {
        static $gFooterShowCount;
        $gFooterShowCount++;
        echo $this->acToggle('GraphShowFooter'.$gFooterShowCount,'<i class="fa fa-cog"></i>');
        ?><div id="GraphShowFooter<?= $gFooterShowCount ?>" style="display:none;" class="box">
        <? $this->pretty($this); ?>
        </div>
        <?php
    }
    
    function pretty($in) {
        print_r($in);
    }
    /*
     * @function fetch
     * @param string type
     * @return string
     * Returns the graph as a string, for template assignment
     */
    function fetch($type = 'ofgraph')
    {
       ob_clean();
       ob_start();
       $this->render($type);
       $html = ob_get_contents();
       ob_end_clean();

       return $html;
    }
    
    function acToggle($div) {
        
    }    
}

class ACPieGraph extends ACGraph {
    var $type = "pie";
}