<?php
/**
 * @version 1.0 Jeff Shiner  1/20/15 2:26 PM
 *
 * @license GPL
 * @version $Id: $
 * @package ActiveCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */

// ini_set('display_errors', 0);
// error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
$useBootstrap = true;
require_once($_SERVER['DOCUMENT_ROOT']."/admin/includes/admin_common.php");
require_once($_SERVER['DOCUMENT_ROOT']."/admin/report/grassroots/php/GrassrootsReport.class.php");

$title = "Home Base Reporting";
if (empty($_REQUEST['headers'])) {
    show_header($title);
}

$r = new GrassrootsReport();
$r->show();

// $r->buildAttributeData();

// $r->buildSurveyData();

//print '<pre>'.print_r($data, TRUE).'</pre>';
// require(dirname(__FILE__)."/html/grassrootsReport.phtml");
