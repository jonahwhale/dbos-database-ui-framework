<?php
/**
 *
 * @todo - Create a test to set up data - JB  1/30/15 10:31 AM
 * @todo - Copy all report data from live server to dev - JB  1/30/15 10:31 AM
 * @task http://activecore.stage.greenpeaceusa.org/admin/bug/admin_bugs.php?bug_id=48744&action=bug_summary
 * 
 * @license GPL
 * @version $Id: $
 * @package ActiveCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */

require_once($_SERVER['DOCUMENT_ROOT']."/admin/report/php/ACReport.class.php");

/**
* @version 1.0 Jonah B  1/30/15 10:32 AM
*/
class GrassrootsReport extends ACTrendReport {
    var $title = "Grassroots Homebase Report";
    var $columnGroups = array();
    var $columnGroupCount = 0;
    /**
    * Array of the data
    */
    var $showFooterTotalRow = false;
    var $data;
    var $_default_view = "monthly";
    
    var $hideChapterSelect = true; // temporary Jonah B  3/23/15 8:08 PM
    
    /**
    * Default number of rows that can be shown on report
    */
    var $rows = 6;
    // survey / question ids
    var $survey_ids = array(
        'id' => 5000,
        'program_id' => 20000,
        'hours_id' => 20001
    );
    
    var $volunteer_attributes = array(342, 343, 344); // 1, 2, 3
    var $greenwire_attributes = array(951); // greenwire
    
    var $attribute_map = array(
        'volunteer_1' => 342,
        'volunteer_2' => 343,
        'volunteer_3' => 344,
        'greenwire' => 951
    );
    
    /**
    * Legacy Jonah B  1/30/15 10:44 AM
    */
    var $data_index = array(
        'volunteer_1' => 0,
        'volunteer_2' => 1,
        'volunteer_3' => 2,
        'volunteer_total' => 3,
        'hours_nan' => 4,
        'hours_field' => 5,
        'hours_frontline' => 6,
        'hours_total' => 7,
        'greenwire' => 8
    );
    
    /**
    * @version 1.0 Jonah B  1/30/15 10:44 AM
    */
    public function __construct() {
    
        $this->addColumnGroup();
        $this->addColumn('Date');
        $this->addColumnGroup("Total Number of Volunteers");
        $this->addColumn(
            array(
                'title'=>'1s',
                "type"=>"attribute",
                "attribute_code"=>"FT1",
                'showTotal'=>true
            )
        );
        $this->addColumn(
            array(
                'title'=>'2s',
                "type"=>"attribute",
                "attribute_code"=>"FT2",
                'showTotal'=>true
            )
        );
        $this->addColumn(
            array(
                'title'=>'3s',
                "type"=>"attribute",
                "attribute_code"=>"FT3",
                'showTotal'=>true
            )
        );
        $this->addColumn(
            array(
                'title'=>'Total <br />(1s, 2s, 3s)',
                'type'=>'equation',
                'columns'=>"1+2+3"
            )
        );
        $this->addColumnGroup("Total Volunteer Hours by Program");
        
        /**
        * NAN Monthly Leadership Reporting
        * This will be comprised of two surveys and will be consolidated into one column.
        * NAN: https://secureusa.greenpeace.org/survey/start/616/ 
        */
        $this->addColumn(
            array(
                'title'=>"NAN Leadership",
                'type'=>'survey',
                'question_title'=>'#volunteer hours for CC\'s and GL\'s (actual)',
                'survey_stub'=>'nanHours1',
                'hidden'=>true
            )
        ); 
        
        /**
        * Community Coach Monthly Reporting Survey - FOR EACH GROUP LEADER
        * http://activecore.stage.greenpeaceusa.org/admin/people/survey/admin_survey.php?action=survey_form&survey_id=607
        */
        
        $this->addColumn(
            array(
                'title'=>"NAN Community",
                'type'=>'survey',
                'question_title'=>'hours volunteered TOTAL by Group Leader AND their entire core team in past month (estimate is fine - total for the whole group)',
                'survey_stub'=>'nanHours2',
                'hidden'=>true
            )
        );     
        
        $this->addColumn(
            array(
                'title'=>"NAN",
                'type'=>'equation',
                'columns'=>'5+6'
            )
        );        
        
        /**
        * http://activecore.stage.greenpeaceusa.org/admin/people/survey/admin_survey.php?action=edit_survey_question&question_id=10667&table=survey_question
        * https://secureusa.greenpeace.org/admin/people/survey/admin_survey.php?action=survey_form&survey_id=614
        */
        $this->addColumn(
            array(
                'title'=>"Field",
                'type'=>'survey',
                'question_title'=>'Volunteer Hours',
                'survey_stub'=>'fieldHours'
            )
        );
        
        /**
        * 661 'Frontline Volunteer Meeting Attendance Sheet'
        */
        $this->addColumn(
            array(
                'title'=>"Frontline",
                'type'=>'survey',
                'question_title'=>'Length of Meeting (Hours)',
                'survey_stub'=>'frontlineHours'
            )
        );
        
        $this->addColumn(
            array(
                'title'=>"Total Hours <br /> (All Programs)",
                'type'=>'equation',
                'columns'=>'5+6+8+9'
            )
        );
        
        $this->addColumnGroup("");
        $this->addColumn(
            array(
                'title'=>"Active <br />Greenwire Users",
                'type'=>'attribute',
                'attribute_code'=>'GWV',
                'showTotal'=>true
            )
        );
    }
    
    /**
    * @version 1.0 Jonah B  3/12/15 9:06 AM
    */
    function getNANSurveyIDs() {
        
    }
    
    /* 
    * @version 1.0 Jonah B  2/12/15 9:06 AM
    * SurveyQuestionType->getQuestions();
    */
    function show() {
        $this->showFilters();
        $this->showTableHeader();
        
        $this->showTableBody($this->report_cols[1]);
        $this->showTableFooter();
        
        $this->showTop10Chapters();
    }
    
    /**
    * @version 1.1 Jonah B  2/2/15 9:53 AM
    * @version 1.0 Jeff S
    */
    function showTop10Chapters() {
        global $zdb_action;
        // top 10 greenwire chapters
        $sqlTopGWChapters = "
            SELECT DISTINCT
                gp_content.chapter.name,
                COUNT(*) AS Total
            FROM gp_action.person
                INNER JOIN gp_content.chapter ON gp_content.chapter.chapter_id = gp_action.person.chapter_id
            WHERE gp_content.chapter.name LIKE '%Greenwire%'
            GROUP BY gp_content.chapter.name
            ORDER BY Total DESC
            LIMIT 10
        ";

        $result = $zdb_action->query($sqlTopGWChapters);
        $rows = $result->fetchAll();
        require(dirname(__FILE__)."/../html/top10Chapter.phtml");
    }
    
    /**
    * @version 1.0 Jonah B  1/30/15 11:32 AM
    */
    function showTableHeader() {
        // da($this->columnGroups);
        ?>
        <div class="alert" style="width:450px;float:right;"><i class="fa fa-warning"></i>Todo: Fix Total 1 2 3 column, fix drilldowns, add border to table bottom and left, make NAN hidden columns expandable</div>

        <br /><div id="reportWrapper">

        <h1><?= $this->title ?></h1> <br />
        <br />
        
        <table class="reportTable table table-striped">
            <thead style="border:2px solid #CCC">
                <tr class="tableHeader">
                
                <?php
                foreach($this->columnGroups AS $c=>$cg) {
                    ob_start();
                    ?><td colspan="<?= $cg->columnCount ?>" align="center"><?= $cg->title ?></td><?php
                    
                    $this->headerRow1 .= ob_get_clean();
                    foreach($cg->columns AS $current => $column) {
                        
                        $this->headerRow2 .= $column->makeHeaderCell();
                    }
                }
                echo $this->headerRow1;
                ?></tr>
                <tr id="reportHeaderColumn2">
                    <?= $this->headerRow2 ?>
                </tr>
            </thead><?php
    }
    
    function showTableFooter() {
        ?>
        
        </table>
        </div><!-- end reportWrapper --><?php
        // da($this->data);
    }
    
    /**
    * @version 1.0 Jonah B  2/2/15 9:55 AM
    */
    function addColumnGroup($title=false) {
        $this->columnGroupCount++;
        $this->columnGroups[$this->columnGroupCount] = new ColumnGroup($title);
        $this->activeColumnGroup = $title;
        
    }
    
    /**
    * @version 1.0 Jonah B  2/2/15 9:55 AM
    */
    function addColumn($title) {

        $this->columnCount++;
        $this->columns[] = $title;
        $this->columnGroups[$this->columnGroupCount]->addColumn($title);
    }
    
}

/**
* @version 1.0 Jonah B  1/30/15 10:29 AM
*/
class ColumnGroup {
    var $columnCount = 0;
    var $title;
    public function __construct($title) {
        $this->title = $title;
    }
    
    public function addColumn($title) {
        $this->columnCount++;
        $this->columns[] = ReportColumn::factory($title);
    }
}

