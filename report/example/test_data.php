<?php
/**
 * @version 1.0 Jeff Shiner  1/20/15 2:50 PM
 *
 * @license GPL
 * @version $Id: $
 * @package ActiveCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */

ini_set('display_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
require_once($_SERVER['DOCUMENT_ROOT']."/admin/includes/admin_common.php");

$survey_ids = array(
    'id' => 5000,
    'program_id' => 20000,
    'hours_id' => 20001,
    'date_id' => 20002
);

if (isset($_GET['delete']))
{
    $sql = "DELETE FROM gp_action.survey_response WHERE admin_user = 'HomeBaseTest' AND survey_id = ?";
    $zdb_action->query($sql, array($survey_ids['id']));
    
    $sql = "DELETE FROM gp_action.survey_response_date WHERE question_id = ?";
    $zdb_action->query($sql, array($survey_ids['date_id']));
    
    $sql = "DELETE FROM gp_action.survey_response_input WHERE FIND_IN_SET(question_id, ?)";
    $zdb_action->query($sql, array(implode(',', array($survey_ids['program_id'], $survey_ids['hours_id']))));
    
    $sql = "DELETE FROM gp_action.person_to_attribute WHERE created_by = 'HomeBaseTest'";
    $zdb_action->query($sql);
    
    print 'test data deleted';
    exit;
}

$attributes = array(342, 343, 344, 951); // 1, 2, 3, greenwire

$departments = array(
    'NAN',
    'Field',
    'Frontline'
);

print 'hours test data:<br />';
for ($i = 0; $i < 100; $i++)
{
        $sql = "
    INSERT INTO 
        gp_action.survey_response (survey_id, archived, admin_user, note, created) 
    VALUES (?, 0, 'HomeBaseTest', '', NOW())
    ";
    $zdb_action->query($sql, array($survey_ids['id']));
    
    $sql = "
        SELECT response_id
        FROM gp_action.survey_response
        WHERE survey_id = ?
        ORDER BY response_id DESC
        LIMIT 1
    ";

    $result = $zdb_action->query($sql, array($survey_ids['id']));
    $row = $result->fetch();
    $response_id = $row['response_id'];

    $random_date = date('Y-m-d H:i:s', mktime(0, 0, 0, mt_rand(1, 12), 1, 2014));
    $random_hours = mt_rand(100, 500);
    $random_department = $departments[mt_rand(0, 2)];

    print 'inserted: '.$response_id.': '.$random_date.' | '.$random_hours.' | '.$random_department.'<br />';

    $sql = "
        INSERT INTO gp_action.survey_response_date (response_id, question_id, response_date, created)
        VALUES (".$response_id.", ".$survey_ids['date_id'].", '".$random_date."', NOW())
    ";
    $zdb_action->query($sql);

    $sql = "
        INSERT INTO gp_action.survey_response_input (response_id, question_id, response_input_value, created)
        VALUES
        (".$response_id.", ".$survey_ids['hours_id'].", ".$random_hours.", NOW()),
        (".$response_id.", ".$survey_ids['program_id'].", '".$random_department."', NOW())
    ";
    $zdb_action->query($sql);
}

$random_person_ids = array();
while (count($random_person_ids) < 1000)
{
    $person_id = mt_rand(400000, 500000);
    if (!in_array($person_id, $random_person_ids))
        $random_person_ids[] = $person_id;
}

print 'volunteer test data:<br />';
for ($i = 0; $i < count($random_person_ids); $i++)
{
    $random_attribute_id = $attributes[mt_rand(0, 3)];
    $random_date = date('Y-m-d H:i:s', mktime(0, 0, 0, mt_rand(1, 12), 1, 2014));

    print 'inserted: '.$random_person_ids[$i].': '.$random_date.' | '.$random_attribute_id.'<br />';

    $sql = "
        INSERT INTO gp_action.person_to_attribute (person_id, attribute_id, created, created_by)
        VALUES (".$random_person_ids[$i].", ".$random_attribute_id.", '".$random_date."', 'HomeBaseTest')
    ";

    $zdb_action->query($sql);
}
