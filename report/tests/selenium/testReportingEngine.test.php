<?php

/**
* Required for standalone
*/
require_once("PHPUnit/Extensions/SeleniumTestCase.php");

if(file_exists("admin/utilities/tests/base/ACSeleniumTestCase.class.php")) {
    require_once("admin/utilities/tests/base/ACSeleniumTestCase.class.php");
    require_once("common.php");
    $standalone = true;
}

/**
* @version 1.0 JB - Just a starting point
*/
class testReportingEngine extends ACSeleniumTestCase {
    var $name = "testReportingEngineRun";

    function testReportingEngineRun() {
        echo("\n\n== Test Reporting Engine ==");
        echo("\n* Priority: Medium");
        echo("\n* Assertions: 5");
        echo("\n* Test started at ".date("Y-m-d H:i:s"));
        $this->logIn();

        $this->open("/admin/people/admin_person.php?action=show_people");
        $this->captureScreenshot("reportingEngineShowPeople.png");

        echo("\n* Make sure someone is in a chapter");
        $this->click("link=Summary");
        $this->waitForPageToLoad();
        $person_id = $this->getValue("person_id");

        // $this->open("/admin/people/admin_person.php?action=person_summary&person_id=216765");
        $this->click("chapter_id_".$person_id."_pid_".$person_id);
        $this->select("inPlaceSelect", "label=activecore");
        $this->click("//option[@value='12']");
        sleep(1);
        $this->captureScreenshot("reportingEngineDBOSChangeChapter.png");

        $this->click("link=Refresh");
        $this->waitForPageToLoad();
        $this->captureScreenshot("reportingEnginePersonToChapter.png");

        echo("\n* See if basic columns are present");
        $this->open("/admin/report/admin_report.php");

        $this->click("link=List Columns");

        $this->waitForPageToLoad("30000");
        $this->captureScreenshot("reportingEngineListColumns.png");
        if(!$this->isTextPresent("Total People Updated")) {
            $this->click("report_column_gp_action");
            $this->waitForPageToLoad("30000");
            $this->captureScreenshot("reportingEngineCreateColumnForm.png");
            echo("\n* Create people updated column");
            $this->type("report_column[title]", "Total People Updated");
            $this->type("report_column[description]", "Total people updated for event rsvp, attribute, person, and person comment by gp staff");
            $this->type("report_column[class]", "PersonChangeLog");
            $this->type("report_column[count_field]", "DISTINCT(person_id)");
            $this->type("report_column[where_sql]", "AND target_table IN ('event_to_person','person_to_attribute','person','person_comment') AND  LEFT(person_change_log.created_by,1) NOT REGEXP '^[0-9]+$'");
            $this->type("report_column[join_sql]", "INNER JOIN person USING (person_id)");
            $this->captureScreenshot("reportingEngineCreateColumn.png");
            $this->click("//input[@value='Save']");
            $this->waitForPageToLoad("30000");

            try {
                $this->assertTrue($this->isTextPresent("Total People Updated"));
            } catch (PHPUnit_Framework_AssertionFailedError $e) {
                array_push($this->verificationErrors, $e->toString()."Column not properly saved.");
            }

            try {
                $this->assertFalse($this->isTextPresent("0000-00-00 00:00:00"));
            } catch (PHPUnit_Framework_AssertionFailedError $e) {
                $error = "ERROR: Modified date set to 0000.  This is a DBOS issue.";
                echo("\n* ".$error);
                array_push($this->verificationErrors, $e->toString().$error);
            }
        } else {
            echo("\n* Total people updated column exists");
        }

        echo("\n* Create a new report");
        $this->open("/admin/report/admin_report.php");
        $this->click("link=Create New Report");
        $this->waitForPageToLoad();
        $report_title = "Test Report ".date("Y-m-d H:i:s");
        $this->type("report[title]", $report_title);

        $this->type("report[sequence]", "1");
        $this->click("//input[@value='Save']");
        $this->waitForPageToLoad();

        try {
            $this->assertTrue($this->isTextPresent($report_title));
        } catch (PHPUnit_Framework_AssertionFailedError $e) {
            array_push($this->verificationErrors, $e->toString()."Report title not found on success page");
        }

        echo("\n* A new report was created");
        $this->click("link=Edit this Report");
        $this->waitForPageToLoad();
        try {
            $this->assertEquals($report_title, $this->getValue("report[title]"));
        } catch (PHPUnit_Framework_AssertionFailedError $e) {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->click("//input[@value='Save']");
        $this->waitForPageToLoad();
        $this->captureScreenshot("reportingEngineAddReportSuccess.png");

        echo("\n* Add a survey column");
        $this->click("add_column");
        $this->waitForPageToLoad();
        $this->captureScreenshot("reportingEngineAddColumn.png");
        $this->select("report_to_column[column_id]", "label=regexp:Another Survey Results\\s");
        $this->type("report_to_column[sequence]", "1");
        $this->click("//input[@value='Save']");
        $this->waitForPageToLoad();
        $this->captureScreenshot("reportingEngineAddColumnSuccess.png");
        /*
        $this->click("link=Add to Report");
        $this->waitForPageToLoad("30000");

        $this->select("report_to_column[report_id]", "label=regexp:Another Report Section\\s");
        $this->type("report_to_column[sequence]", "9");
        $this->click("//input[@value='Save']");
        $this->waitForPageToLoad("30000");
        */

        $this->click("link=".$report_title." >");
        $this->waitForPageToLoad();
        echo("\n* Build the report");
        $this->click("link=Build");
        $this->waitForPageToLoad();
        echo("\n* Now add a date column");
        $this->click("add_column");
        $this->waitForPageToLoad();
        $this->select("report_to_column[column_id]", "label=regexp:Date\\s");
        $this->type("report_to_column[sequence]", "0");
        $this->click("//input[@value='Save']");
        $this->waitForPageToLoad();
        $this->click("//table[@id='ACWrapperTable']/tbody/tr[2]/td[2]/div/table[2]/tbody/tr[6]/td[2]/a");
        $this->click("link=Build");
        try {
            $this->assertTrue($this->isTextPresent("Date"));
        } catch (PHPUnit_Framework_AssertionFailedError $e) {
            array_push($this->verificationErrors, $e->toString()."Date column not found");
        }

        $this->captureScreenshot("reportingEngineTestEnded.png");
        echo("\n* Test ended at ".date("Y-m-d H:i:s"));
    }
}

