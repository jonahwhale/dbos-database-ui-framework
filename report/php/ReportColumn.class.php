<?php
/**
* File containing Admin tools for manipulating report columns.  This tool was developed
* for Greenpeace, Ben Smith in 2009, but never went into production until 2010 for David Pomerantz.
*
* @liscense GPL
* @version $Id: ReportColumn.class.php 8337 2015-04-09 23:30:21Z jbaker $
* @author Jonah Baker  9/30/08 8:40 AM for GreenpeaceUSA
*/

/*
* Admin tools for manipulating report columns.
*
* @version 1.0 Jonah  10/1/08 7:42 PM
*/

class ReportColumnTable extends DBOS {
    var $_db = PERSON_DB;
    var $_table = "report_column";
    var $_idField = "column_id";
    var $_titleField = "name";
    var $report_id = 0;
    /**
    * Default date group and sort field.
    */
    var $_dateField = "created";
    /**
    * The ajax and admin file associated with these records.
    */
    var $_controller = "/admin/report/admin_report.php";

    function __construct() {
        $this->loadTable();
    }

    function showTable($opts=false) {
        parent::showTable(array("ajax"=>true));
    }

    function adminPageTop() {
        parent::adminPageTop();
        ?>
        <? if($_REQUEST['report_id']) { ?>
        | <a class="addMe" id="addColumn" href="<?= $_SERVER[PHP_SELF] ?>?action=edit&table=report_to_column&report_id=<?= $_REQUEST['report_id'] ?>">Add column to report</a>
        <? } ?>
        | <a href="<?= $_SERVER[PHP_SELF] ?>?action=list&table=report">List Reports</a>
        | <a class="refreshMe" href="<?= $_SERVER['PHP_SELF'] ?>">Refresh</a>

        <?php
    }

    function adminActionCell($row) {
        parent::adminActionCell($row);
        ?>
        | <a class="addMe" href="<?= $_SERVER[PHP_SELF] ?>?action=edit&table=report_to_column&column_id=<?= $row['column_id'] ?>">Add to Report</a>
        <?
    }

    /**
    * Major override of adminPage behaviors for colums, since they each have their
    * own class depending on type.
    */
    public function adminPage($action=false) {

        if(!$action) $action = $_REQUEST['action'];
        if(!$table) $table = $_REQUEST['table'];
        // Only supports simple index fields.
        if(!is_array($this->_idField)) {
            if(!$id) $id = $_REQUEST[$this->_idField];
        } else {
            foreach($this->_idField AS $field) {
              $id[$field] = $_REQUEST[$field];
            }
        }

        $this->adminPageTop();
        switch($action) {
            case"delete":
                $obj = ReportColumn::factory($id);
                $obj->delete();
                show_success("The record was delete from ".$this->_table);
                $this->showTable();
                break;

            case"edit_".$this->_table:
            case"edit":
                $obj = ReportColumn::factory($id);
                $obj->showForm();
                break;
            case"db_edit_".$this->_table:
                $obj = ReportColumn::factory($id);
                $id = $obj->saveForm();
                if(!is_numeric($id)) {
                    $this->showTable();
                    break;
                }
            case"summary":
            case $this->_table."_summary":
                $obj = ReportColumn::factory($id);
                // Should be toAdminHTML.
                if(is_numeric($obj->id)) {
                    $filtered_title = camelcaps(str_replace("_"," ",$obj->_table));
                    ?>| <a class="editMe" href="<?= $_SERVER[PHP_SELF] ?>?action=edit&table=<?= $obj->_table ?>&<?= $this->_idField ?>=<?= $obj->id ?>">Edit this <?= $filtered_title ?></a>
                    <?
                }
                $obj->toHTML();
                break;
            case"list":
            case"":
                $this->showAdminTable();
                break;
            default:
              ?>The action <?= $action ?> is not defined for this the <?= $this->_table ?> class.  Use Object->adminPage to set.<?
        }
    }

}


/**
* Report columns are the building blocks of the trend reporting engine.
*
* @version 1.0 Jonah  8/27/08 10:39 AM
*
*/

class ReportColumn extends ReportColumnTable {
    var $_default_view = "weekly";
    var $columnCount = 0;
    var $showTotal = false;
    var $value = false;
    var $values = array();
    var $valueTotal = 0;
    /*
    * This is a weird creator, I might standardize it, works in a very manual way,
    * could be useful. JB  9/30/08 8:32 AM
    * Accepts either an array of the values for the column, or a numberic id of
    * the record that holds those values.
    *
    * @version 1.0 JB  9/2/08 10:36 AM
    * @version 1.1 SJP 01/07/2010 03:36 AM Updated chapter support
    */

    function __construct($opts=false) {

        $this->view = $this->getView($opts);

        if(is_array($opts)) {
            $this->_dbValues = $opts;
            foreach($opts AS $f=>$v) {
              $this->$f = $v;
            }
            if(is_numeric($this->column_id)) {
              $this->id = $this->column_id;
            }
            if($opts['showTotal']) {
                $this->showTotal = true;
            }
            if($opts['hidden']) {
                $this->hidden = true;
            }
        } else {
            if(is_numeric($opts)) {
                $id = $opts;
                unset($opts);
                $this->id = $id;
                $this->loadRecord($id);
            } else { // load title into record, not from db, assuming string of title
                
                $this->_title = $opts;
                $this->name = $opts;
                $this->title = $opts;
                
                unset($opts);
            }
        }

        if(!$this->chapter_id && is_numeric($_REQUEST['chapter_id'])) {
            $this->chapter_id = $_REQUEST['chapter_id'];
        }
        $this->_title = $this->title;

        if($this->date_field) {
            $this->_dateField = $this->date_field;
        }

    }

    
    /**
    * Decide which ReportColumn class extension to create the object with, depending
    * on the 'type' of the column being shown.
    *
    * @version 1.1 SJP 01/07/2010 03:36 AM Passed chatper ID to ReportColumn
    * @version 1.0 Jonah  10/1/08 7:55 PM
    */
    static function factory($opts) {
        global $zdb_action;
        $debug = false;
        if(is_numeric($opts)) {
            $column_id = $opts;
            unset($opts);
            $sql = "SELECT * FROM report_column WHERE column_id = $column_id";
            $opts = $zdb_action->fetchRow($sql);
        } elseif(is_string($opts)) {
            $set['title'] = $opts;
            $set['type'] = '';
            unset($opts);
            $opts = $set;
        } elseif(is_object($opts)) {
            // da($opts);
        }
        
        if(
            !$opts['type'] && 
            ($opts['title']=="Date" || $opts['title'] == "5ate")
        ) {
            $opts['type'] = "date";
        }
        
        switch($opts['type']) {
            case"attribute":
                require_once(dirname(__FILE__)."/ReportColumnAttribute.class.php");
                $rc = new ReportColumnAttribute($opts);
                break;
            case"date":
                require_once(dirname(__FILE__)."/ReportColumnDate.class.php");
                $rc = new ReportColumnDate($opts);
                break;
            case"survey":
                $rc = new ReportColumnSurvey($opts);
                break;
            case"class":
                $rc = new ReportColumnClass($opts);
                break;
            case"equation":
                $rc = new ReportColumnEquation($opts);
                break;
            default:
                $rc = new ReportColumn(array("chapter_id"=>$opts['chapter_id']));
                if(is_array($opts)) {
                    foreach($opts AS $f=>$v) {
                        $rc->$f = $v;
                    }
                } else {
                  ob_start();
                  ?>Warning: build failed in ReportColumn::factory for <? da($opts); 
                  ob_get_clean();
                }
        }
        $rc->type = $opts['type'];
        $rc->_opts = $opts;
        if($debug) da($rc);
        
        return $rc;
    }

    function getView($opts=false) {
        if($_REQUEST['view']) {
            $this->view = $_REQUEST['view'];
        } elseif($opts['default_view']) {
            $this->view = $opts['default_view'];
        } else {
            $this->view = $this->_default_view;
        }
        return $this->view;
    }

    /**
    * @version 1.0 Jonah B  3/30/15 3:02 PM
    */
    function makeHeaderCell() {
        ob_start();
        ?><td class="makeHeaderCell"><?= $this->title ?> </td><?php
        return ob_get_clean();
    }

    /**
    * Cusomize the display of some form fields for ReportColumn admin.
    *
    * @version Jonah  10/1/08 7:56 PM
    */

    function formField($name,$opts=false) {
        global $zdb_action;
        switch($name['field']) {
           case"type_value":
             ob_start();
             acToggle("choose_columns","Select Column Ids");
             $sql = "SELECT column_id, title FROM report_column WHERE type='class' ";
             $cols = $zdb_action->getAll($sql);
             ?>
             <div id="choose_columns" style="position:absolute;margin-left:100px;background:#FFF;
                border:1px solid #CCCCCC;display:none;width:400px;padding:7px;">
                Copy/paste the column ids below to create your equation.  Currently only 'lookup'
                or 'class' field types are supported for equations.
                An example equation would look like "C27+C35".

              <p>Here is a list of supported columns, with their ids</p>
             <? foreach($cols AS $col) { ?>
                 C<?= $col['column_id'] ?> - <?= $col['title'] ?><br />
             <? } ?>
             </div><?
             $footer = ob_get_clean();
             parent::formField($name,array("footer"=>$footer));
             break;
           default:
             parent::formField($name,$opts);
        }
    }

    /*
    * Vars to hand in the url for the number displayed in the report.
    *
    * @version 1.0 JB  10/7/08 10:27 AM
    */

    function urlVars() {
        $urlVars .= "?chapter_id=";
        if($this->chapter_id) {
            $urlVars .= $this->chapter_id;
        } else {
            $urlVars .= $_REQUEST['chapter_id'];
        }

        $urlVars .= "&column_id=".$this->column_id;
        if($this->survey_id) {
            $urlVars .= "&survey_id=".$this->survey_id;
        }
        if(isset($this->row)) $urlVars .= "&row=".$this->row;
        if(is_object($this->object)) {
            $urlVars .= "&table=".$this->object->_table;
            // $urlVars .= "&id=$this->object->id";
        }
        $urlVars .= "&action=list";
        $urlVars .= "&view=".$this->view;
        if($this->url_passvar) {
            $urlVars .= $this->url_passvar;
        }
        $this->urlVars = $urlVars;
        return $urlVars;
    }

    /*
    * Admin summary for this column metadata.
    */
    function toHTML() {
        $debug = true;
        ?><br /><?
        $this->adminActionCell($this->_dbValues);
        parent::toHTML();
        if($debug) echo $this->view;
        $this->adminActionCell($this->_dbValues);
        if($this->query_id) {
            $q = new PersonQuery($this->query_id);
            ?><br />This column uses the saved search <b><?= $q->title ?></b>
            <?= $q->summary_link(); ?>
            <br />
            <?
            da($q->sql_query);
            $parts = $q->getSqlParts();

            $vars = urldecode($q->http_query);
            if($debug) da($vars);
        }

        $type = ReportColumn::factory($this->_dbValues);

        if($this->type=="class") {
            ?><br /><br />The trendSql for this column looks like:<?
//            $sql = $type->trendSql();
//            da($sql);
            $sql_opts = array("where"=>$this->where_sql,"field"=>$this->field,"join"=>$this->join_sql,"having"=>$this->having_sql,"date_field"=>$type->object->_dateField);
            $sql = $this->object->getTrendSql($sql_opts);
            da($sql);
            ?><br /><br />Rebuilding it looks like:<br /><?
             $g = group_time($this->view,$type->object->_dateField,array("table"=>$type->object->_table));
            // $g = group_time($view,$type->object->_dateField,array("table"=>"p2att_1"));
            ?><br /><br />Saved search parts:<br><?
            da($parts);

            ?>date field grouping:<?
            da($g);

            $select = " SELECT COUNT(*) AS count ,".$g[date_field];
            $from = " FROM ".$parts[from];
            $where = " WHERE ".$parts[where]." ";
            $group_by = " ".$g[sql];
            $sql = $select.$from.$where.$group_by;
            ?>Ta da!:<br /><?
            da($sql);
        }
        // Show all reports
        $rep = new ACReportTable();
        // $rep->showTable();
        
        $rtq = new ReportToColumn();
        $rtq->addFilter(array("column_id",$this->column_id));
        $rtq->showTable();
    }

    /*
    *  Doesn't do anything, was trying to process a saved search query into editable parts.  JB  9/30/08 8:59 AM
    */

    function trendSql() {
        if($this->query_id) {
           $q = new PersonQuery($this->query_id);
           $raw = $q->sql_query;
           // Hope this works!
           list($good,$bad) = explode("GROUP BY",$sql);

        }
    }
    
    /**
    *
    * @version 1.1 SJP 01/20/2010 08:44 PM Added support to return value
    * @version 1.0 Jonah  9/3/08 12:57 PM
    * @todo ReportRow should probably be its own class. - It should be called showCell JB  9/3/08 12:58 PM
    */
    function showCell($num,$opts=false) {
        if($opts && is_array($opts)) extract($opts);
        
        $this->view = $this->getView($opts);
        $this->columnCount++;
        $this->currentRow = $num;
        
        $cell_key = 'cell_key_'.$this->report_id."_".$this->columnCount."_".$num."_".dirify($this->title);
        
        if($table_id) $cell_key .= "_".$table_id;

        $this->row = $num;
        // $num can be interpreted as rows from top.  So if the view is weekly,
        //  assume  TO_WEEKS(date) = (TO_WEEKS(NOW()) - $num)
        // Why isn't the following being overridden by the ReportColumnClass object?
        if(
            is_object($this->object) 
            && ($this->type=="class" || $this->type=="attribute" || $this->type=="survey")
        ) {
            // @todo utilize ->where_sql field.
            $sql_opts = array("where"=>$this->where_sql,"join"=>$this->join_sql,"having"=>$this->having_sql,"date_field"=>$this->object->_dateField);
            if($this->chapter_id) $sql_opts['chapter_id'] = $this->chapter_id;
            if($this->count_field) $sql_opts['count_field'] = $this->count_field;
            if(!$this->setValue) {
                $this->value = $this->object->getTrendCell(
                    $num,
                    $this->field,
                    $this->view,
                    $sql_opts
                );
                if($opts['startingTotal']) {
                    $setValue = $opts['startingTotal'] - $this->valueTotal;
                    
                }                
                $this->values[] = $this->value;
                $this->valueTotal = $this->valueTotal + $this->value;  
                if($setValue) {
                    $this->value = $setValue;
                }
                
            } else {
                $this->value = $this->setValue;
            }
            
            // da($this->object->sql);
        }
        
        $this->startCell();
        if(!$this->hidden) {
            if(isset($this->value)) {
    
              ?><span id="<?= $cell_key ?>_value"><? echo $this->value; ?></span><?php
            } else {
                if($this->verbose) {
              ?><span class="caption" id="notDefinedError"><?= $this->title ?> Not Defined</span><? 
              } else {
              ?><span class="caption" id="notDefinedError">*</span><?
              }
            }
    
            // da($this->object->_currentDateRange);
    
            if($this->object->_dateField) {
                $date_field = "&date_field=".$this->object->_dateField."&date_starting=".$this->object->_currentDateRange['sql_starting']."&date_ending=".$this->object->_currentDateRange['sql_ending'];
            }
            ?>
            <span class="smalltype print" style="font-size:10px!important;size:3px;">
            <?= acToggle($cell_key,''); ?>
            </span>
            <div id="<?= $cell_key ?>" style="display:none;">
                <a title="Show records" id="drilldownLink" href="<?= $this->object->_controller ?><?= $this->urlVars() ?><?= $date_field ?>"><i class="fa fa-list"></i> List</a>
                <br />
                <?
            
                if($this->showCellSQL=true) {
                    ?>
                    <a class="acToggleHref" href="javascript:showLevel( '<?= $cell_key ?>_sql', '<?= $cell_key ?>Img_sql');"> 
                        <i id='<?= $cell_key ?>Img_sql' class="fa fa-cog"> SQL</i>
                    </a>
                    <div id="<?= $cell_key ?>_sql" style="display:none;">
                        <pre><?
                    echo $this->object->sql;
                    ?></pre>
                    </div><?
                }
            ?></div><?php
        }
        $this->endCell();
        $this->data[$row][$this->columnCount]=$this->value;
        global $data;
        $data = $this->data;
        return $this->value;
    }

    /**
    * A goal cell that will accept manually entered numbers fed from the survey engine.
    *
    * - row is the numeric value of the row.
    * - opts
    * You can manually pass a question_title, which will overrie the addition of ' Goal' to the
    * column title.
    *
    * @version 1.0 JB  8/25/08 1:03 PM
    * @function ReportColumn->showGoalCell($row)
    */

    function showGoalCell($row,$opts=false) {
        // At this point we could do the survey question lookup.
        if($opts) extract($opts);

        // You can hand in survey_id as opts to override the column survey setting.
        if($this-survey_id && !$survey_id) $survey_id = $this->survey_id;

        if(!$this->survey_stub && !$this->survey_id && !$survey_id && $this->use_goal) {
            // @todo - Inherit survey id from parent report.
            /* ?>The question <?= $this->title ?> has no survey specified.  Please fix.<? */
        }

        if(!$question_title) {
            if(!$this->question_title) {
                $question_title = $this->title." Goal";
            } else {
                $question_title = $this->question_title." Goal";
            }
        }

        if(!$this->surveyObj) {

            if($this->survey_stub) {
                $this->surveyObj = Survey::get_by_tag($this->survey_stub);
            } elseif($survey_id) {
                $this->surveyObj = new Survey($survey_id);
            }

        }

        if(is_numeric($this->surveyObj->survey_id)) {
            $survey_id = $this->surveyObj->survey_id;
        }

        if(!$this->response_id && is_object($this->surveyObj)) {
            $this->response_id = $this->surveyObj->getResponseByRow($row);
            if(!$this->question_id) {
                $this->question_id = $this->surveyObj->getQuestionByTitle($question_title);
            }

            if($this->question_id && $this->response_id) { // Found it!
                $srq = new SurveyResponseQuestion($this->question_id,$this->response_id);
                $goal = $srq->getValue();

                $no_value_message = "not set";
            } elseif(!$this->question_id && $this->use_goal) {
                // They want to use a goal, but there is no question for it.
                // Could not find the survey question for this goal.
                if($this->use_goal) {
                    $no_value_message = "no question for $question_title";
                }
                // Attempt to create the question on-the-fly
                if($survey_id && $this->use_goal) {
                    $sq = new SurveyQuestion("new");
                    $sq->survey_id = $survey_id;
                    $sq->question_label = $question_title;
                    $sq->question_type = 12; // hack I know.  @todo - Do a lookup of the 'float' question type.
                    // Disabled for now, be very careful using this.  JB  12/22/08 7:48 PM
                    // $sq->save();
                }
            }
        }
        if($get_value) return $goal;
        // Begin output
        if($this->type=="date") {
           ?><td align="right">Goals:</td><?
        } else {

            ?><td align="right" class="goalCell">
            <? if($this->use_goal) {
                if($goal) {
                  echo $goal;
                } else {
                ?>
                   <span class="caption"><?= $no_value_message ?></span>
               <? } ?>

               <a target="new" id="viewSurveyResponse" title="View Survey Response" href="/admin/people/survey/admin_survey.php?action=edit_response&survey_id=<?= $this->surveyObj->survey_id ?>&view=<?= $this->view ?>&row=<?= $row ?>&chapter_id=<?= $this->chapter_id ?>">&gt;</a>
            <? } ?>
            </td>
            <?
        }
    }

    function startCell($opts=false) {
        if(is_array($opts)) extract($opts);
        if($class) {
            $html_class = " class=\"".$class."\" ";
        } else {
            $html_class = " class=\"".$this->type."\" ";
        }
        if($this->hidden) {
            $style = "style='display:none;'";
        }
        ?><td id="row<?= $num ?>" <?= $html_class ?>  align="right" <? if($nobreak) echo "nowrap"; ?>>
        <span class="startCellContentWrapper" <?= $style ?>>
        <?
    }

    function endCell() {
      ?>
      </span>
      </td><?
    }

}

/**
* Include Equation Column type
*/
include_once(dirname(__FILE__)."/ReportColumnEquation.class.php");

/**
* Include survey question type.
*/
include_once(dirname(__FILE__)."/ReportColumnSurvey.class.php");


/**
 * This class handles the main 'lookup' column type which relies on a class existing
 * that is an extention of the DBOS database object. JB  9/30/08 9:20 AM
 *
 * @version 1.0 2008 Jonah B
 */

class ReportColumnClass extends ReportColumn {

    function __construct($opts=false) {
        parent::__construct($opts);
        if($opts['class']) {
            $tclass = $opts['class']."Table";
            // Create the object that corresponds to this columns class.
            $this->object = new $tclass;
            if($opts['date_field']) {
                $this->object->_dateField = $opts['date_field'];
            }
        } else {
            show_errors("No class specified for lookup column $this->title, please fix.");
            @$this->summary_link();
        }
        if($id) {
            $this->defaultFilter($id);
        }
        // $this->value = $this->object->getTrendCell(0,$opts['field']);

    }

    /*
    * Inverval is the 'row' where we want the value from.  A value of 3 in a weekly
    * report would mean give me the value from 3 weeks ago.
    */
    function getCellValue($interval,$opts=false) {
        // A cache would be a good idea here.  Since there are equation column types
        // it's possible that this will be called multiple times in a script. JB  9/30/08 9:21 AM
        // A useful key would be column_id+row
        if($debug) da($this);
        $this->value = $this->object->getTrendCell(
            $interval,
            $this->field,
            "weekly",
            array(
                "where"=>$this->where_sql,
                "join"=>$this->join_sql,
                "having"=>$this->having_sql)
        );
        return $this->value;
    }


}  // end ReportColumnClass


/**
 * The links between columns and tables, for admin editing.
 *
 * @function ReportToColumnTable
 * @author Jonah  10/1/08 7:40 PM
 */

class ReportToColumnTable extends DBOS {
    var $_db = PERSON_DB;
    var $_table = "report_to_column";
    var $_idField = array("report_id","column_id");
    var $_controller = "/admin/report/admin_report.php";

    function __construct() {
        /*
        * Set name display field for column -- SJP 09/02/2010 06:55PM
        */
        /*
        $select_field['column_id'] = "CONCAT(title,' (',description,') ')";
        $this->select_field = $select_field;
        */
        $this->loadTable();
    }

    function showTable($opts=false) {
        $opts['ajax'] = true;
        parent::showTable($opts);
    }

    function adminPageTop() {
        parent::adminPageTop();
        ?> | <a id="listAllReports" href="<?= $_SERVER['PHP_SELF'] ?>">List All Reports</a><?
        ?> | <a id="reportColumnListColumns" href="<?= $_SERVER['PHP_SELF'] ?>?action=list&table=report_column">List Columns</a><?
    }
}

/**
* @author Jonah  10/14/09 3:44 PM
* @package ActiveCore
*/
class ReportToColumn extends ReportToColumnTable {

    /**
    *
    */
    function __construct($ids,$opts=false) {
        global $session_user;
        $this->loadRecord($ids);
        if(!$this->modified_by) $this->modified_by = $session_user->username;
    }


}
