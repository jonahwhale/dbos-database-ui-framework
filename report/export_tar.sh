#!/bin/sh
SYSTEMDATE=`date '+%y%m%d'`

# maybe use pear package package.xml instead of this.  JB  10/16/08 3:47 PM
php -q install/index.php
echo
echo Tar up files on $SYSTEMDATE
tar --create --verbose --exclude-from=install/export_tar_exclude --file=install/snaps/$SYSTEMDATE-ACReportingEngine.tar .
